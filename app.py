#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

from flask import Flask, render_template, request
from sqlalchemy.sql.expression import func
from sqlalchemy import desc, asc
from flask_migrate import Migrate, migrate, upgrade

import logging
from logging import Formatter, FileHandler
import os
from urllib.parse import urlparse

import datetime

from threading import Thread
from functools import wraps

import time
import atexit

from apscheduler.schedulers.background import BackgroundScheduler

import requests

from html.parser import HTMLParser

from conf import config

from apiv1 import api_v1

from classes.shared import db
from classes import topics
from classes import streamers
from classes import servers
from classes import channels
from classes import streams
from classes import videos
from classes import clips

#----------------------------------------------------------------------------#
# App Config.
#----------------------------------------------------------------------------#
version = "alpha-1"

app = Flask(__name__)

app.jinja_env.cache = {}

app.config['SECRET_KEY'] = config.secretKey
app.config['SQLALCHEMY_DATABASE_URI'] = config.dbLocation
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
if config.dbLocation[:6] != "sqlite":
    app.config['SQLALCHEMY_MAX_OVERFLOW'] = -1
    app.config['SQLALCHEMY_POOL_RECYCLE'] = 1600
    app.config['MYSQL_DATABASE_CHARSET'] = "utf8"
    app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'encoding': 'utf8', 'pool_use_lifo': 'True', 'pool_size': 20}
else:
    pass

db.init_app(app)
db.app = app
migrateObj = Migrate(app, db)

db.create_all()

app.register_blueprint(api_v1)

#----------------------------------------------------------------------------#
# Functions and Decorators.
#----------------------------------------------------------------------------#

# Automatically tear down SQLAlchemy.
@app.teardown_request
def shutdown_session(exception=None):
    db.session.remove()

def asynch(func):
    @wraps(func)
    def async_func(*args, **kwargs):
        func_hl = Thread(target = func, args = args, kwargs = kwargs)
        func_hl.start()
        return func_hl
    return async_func

class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_html(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

#@asynch
# TODO Fix Async Firing but not saving
def validateServer(server):
    success = False
    r = None

    protocol = 'https://'

    try:
        r = requests.get(protocol + server.siteAddress + '/apiv1/server')
        success = True
        server.siteProtocol = "https://"
    except requests.exceptions.Timeout:
        pass
    except requests.exceptions.ConnectionError:
        pass

    if success == False:
        protocol = 'http://'
        try:
            r = requests.get(protocol + server.siteAddress + '/apiv1/server')
            success = True
            server.siteProtocol = "http://"
        except requests.exceptions.Timeout:
            pass
        except requests.exceptions.ConnectionError:
            pass

    if r.status_code == 200 and success is True:
        serverJSON = r.json()['results']
        server.siteName = serverJSON['siteName']
        server.siteLogo = serverJSON['siteLogo']
        server.serverMessage = serverJSON['serverMessage']
        server.version = serverJSON['version']
        server.lastSeen = datetime.datetime.utcnow()
        serverToken = server.validate()

        apiEndpoint = "apiv1"

        try:
            r = requests.post(protocol + server.siteAddress + '/' + apiEndpoint + '/hub/validateServer', data={'verificationToken': server.verificationToken, 'serverToken': serverToken})
        except requests.exceptions.Timeout:
            return False
        except requests.exceptions.ConnectionError:
            return False
        if r.status_code == 200:
            db.session.commit()
            return True
    return False

def checkUnverifiedServers():
    pendingServersQuery = servers.servers.query.filter_by(validated=False).all()
    checklist = []
    for server in pendingServersQuery:
        result = validateServer(server)
        checklist.append(result)
    return checklist

#----------------------------------------------------------------------------#
# Scheduler Tasks
#----------------------------------------------------------------------------#

scheduler = BackgroundScheduler()
scheduler.add_job(func=checkUnverifiedServers, trigger="interval", seconds=60)
scheduler.start()

#----------------------------------------------------------------------------#
# Template Filters
#----------------------------------------------------------------------------#

@app.template_filter('normalize_uuid')
def normalize_uuid(uuidstr):
    return uuidstr.replace("-", "")

@app.template_filter('normalize_urlroot')
def normalize_urlroot(urlString):
    parsedURLRoot = urlparse(urlString)
    URLProtocol = None
    if parsedURLRoot.port == 80:
        URLProtocol = "http"
    elif parsedURLRoot.port == 443:
        URLProtocol = "https"
    else:
        URLProtocol = parsedURLRoot.scheme
    reparsedString = str(URLProtocol) + "://" + str(parsedURLRoot.hostname)
    return str(reparsedString)

@app.template_filter('normalize_url')
def normalize_url(urlString):
    parsedURL = urlparse(urlString)
    if parsedURL.port == 80:
        URLProtocol = "http"
    elif parsedURL.port == 443:
        URLProtocol = "https"
    else:
        URLProtocol = parsedURL.scheme
    reparsedString = str(URLProtocol) + "://" + str(parsedURL.hostname) + str(parsedURL.path)
    return str(reparsedString)

@app.template_filter('normalize_date')
def normalize_date(dateStr):
    return str(dateStr)[:19]

@app.template_filter('limit_title')
def limit_title(titleStr):
    if len(titleStr) > 40:
        return titleStr[:37] + "..."
    else:
        return titleStr

@app.template_filter('hms_format')
def hms_format(seconds):
    val = "Unknown"
    if seconds != None:
        seconds = int(seconds)
        val = time.strftime("%H:%M:%S", time.gmtime(seconds))
    return val

@app.template_filter('get_topicName')
def get_topicName(hubID):
    topicQuery = topics.topics.query.filter_by(hubID=hubID).first()
    if topicQuery == None:
        return "None"
    return topicQuery.name


@app.template_filter('get_userName')
def get_userName(hubID):
    userQuery = streamers.streamers.query.filter_by(hubID=hubID).first()
    return userQuery.username

#----------------------------------------------------------------------------#
# Route Controllers.
#----------------------------------------------------------------------------#

@app.route('/')
def main_page():
    channelList = channels.channels.query.order_by(channels.channels.views.desc()).limit(6)
    streamList = streams.streams.query.order_by(streams.streams.currentViewers.desc()).limit(12)
    videoList = videos.videos.query.order_by(videos.videos.views.desc()).limit(12)
    clipList = clips.clips.query.order_by(clips.clips.views.desc()).limit(12)

    return render_template('index.html', channelList=channelList, streamList=streamList, videoList=videoList, clipList=clipList)

@app.route('/servers')
def servers_page():
    render_template('servers.html')

@app.route('/channels')
def channels_page():
    channelList = channels.channels.query.order_by(channels.channels.views.desc()).all()
    return render_template('channels.html', channelList=channelList)

@app.route('/topics')
def topics_page():
    topicsList = []
    for topic in topics.topics.query.with_entities(topics.topics.name).distinct():
        topic = topic[0]
        topicsList.append(str(topic))
    topicsList.sort()
    return render_template('topics.html', topicList=topicsList)

@app.route('/topics/<topicName>/')
def topicSearch_page(topicName):
    topicsQuery = topics.topics.query.filter(topics.topics.name.like(topicName)).all()
    topicHubIDList = []
    for topic in topicsQuery:
        topicHubIDList.append(topic.hubID)

    channelList = []
    videoList = []
    streamList = []
    clipList = []

    for hubID in topicHubIDList:
        channelQuery = channels.channels.query.filter_by(topic=hubID).all()
        for channel in channelQuery:
            channelList.append(channel)
        videoQuery = videos.videos.query.filter_by(topic=hubID).all()
        for video in videoQuery:
            for clip in video.clips:
                clipList.append(clip)
            videoList.append(video)
        streamQuery = streams.streams.query.filter_by(topic=hubID).all()
        for stream in streamQuery:
            streamList.append(stream)
    return render_template('topicView.html', channelList=channelList, videoList=videoList, streamList=streamList, clipList=clipList)

@app.route('/videos')
def videos_page():
    videoList = videos.videos.query.order_by(videos.videos.views.desc()).all()
    return render_template('videos.html', videoList=videoList)

@app.route('/streams')
def streams_page():
    streamList = streams.streams.query.order_by(streams.streams.currentViewers.desc()).all()
    return render_template('streams.html', streamList=streamList)

@app.route('/clips')
def clips_page():
    clipList = clips.clips.query.order_by(clips.clips.views.desc()).all()
    return render_template('clips.html', clipList=clipList)

@app.route('/streamers')
def streamers_page():
    streamerList = streamers.streamers.query.order_by(streamers.streamers.username).all()
    return render_template('streamers.html', streamerList=streamerList)

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)