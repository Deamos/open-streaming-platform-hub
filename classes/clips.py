from .shared import db

class clips(db.Model):
    __tablename__ = "clips"
    id = db.Column(db.Integer, primary_key=True)
    hubID = db.Column(db.String(255), unique=True)
    serverID = db.Column(db.Integer, db.ForeignKey('servers.id'))
    name = db.Column(db.String(255))
    parentVideo = db.Column(db.String(1024), db.ForeignKey('videos.hubID'))
    length = db.Column(db.Float)
    location = db.Column(db.String(255))
    views = db.Column(db.Integer)
    img = db.Column(db.String(255))
    description = db.Column(db.String(2056))
    upvotes = db.Column(db.Integer)

    def __init__(self, hubID, server, name, location, parentVideo, views, length, img, description, upvotes):
        self.hubID = hubID
        self.serverID = server
        self.name = name
        self.location = location
        self.parentVideo = parentVideo
        self.length = length
        self.views = views
        self.img = img
        self.description = description
        self.upvotes = upvotes

    def __repr__(self):
        return '<id %r>' % self.id
