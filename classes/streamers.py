from .shared import db

class streamers(db.Model):
    __tablename__ = "streamers"
    id = db.Column(db.Integer, primary_key=True)
    hubID = db.Column(db.String(255), unique=True)
    serverID = db.Column(db.Integer, db.ForeignKey('servers.id'))
    username = db.Column(db.String(255))
    biography = db.Column(db.String(2056))
    img = db.Column(db.String(256))
    location = db.Column(db.String(256))

    channels = db.relationship('channels', backref='owner', lazy="joined")

    def __init__(self, hubID, server, username, biography, img, location):
        self.hubID = hubID
        self.serverID = server
        self.username = username
        self.biography = biography
        self.img = img
        self.location = location

    def __repr__(self):
        return '<id %r>' % self.id
