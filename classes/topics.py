from .shared import db

class topics(db.Model):
    __tablename__ = "topics"
    id = db.Column(db.Integer, primary_key=True)
    hubID = db.Column(db.String(255), unique=True)
    serverID = db.Column(db.Integer, db.ForeignKey('servers.id'))
    name = db.Column(db.String(255))
    img = db.Column(db.String(255))

    def __init__(self, hubID, server, name, img):
        self.hubID = hubID
        self.serverID = server
        self.name = name
        self.img = img

    def __repr__(self):
        return '<id %r>' % self.id
