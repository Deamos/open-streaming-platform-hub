from .shared import db

class videos(db.Model):
    __tablename__ = "videos"
    id = db.Column(db.Integer, primary_key=True)
    hubID = db.Column(db.String(255), unique=True)
    serverID = db.Column(db.Integer, db.ForeignKey('servers.id'))
    name = db.Column(db.String(255))
    streamer = db.Column(db.String(1024), db.ForeignKey('streamers.hubID'))
    channelID = db.Column(db.String(1024), db.ForeignKey('channels.hubID'))
    topic = db.Column(db.String(1024), db.ForeignKey('topics.hubID'))
    length = db.Column(db.Float)
    location = db.Column(db.String(255))
    views = db.Column(db.Integer)
    img = db.Column(db.String(255))
    description = db.Column(db.String(2056))
    upvotes = db.Column(db.Integer)

    clips = db.relationship('clips', backref='video', lazy="joined")

    def __init__(self, hubID, server, name, location, streamer, topic, channelID, views, length, img, description, upvotes):
        self.hubID = hubID
        self.serverID = server
        self.name = name
        self.location = location
        self.streamer = streamer
        self.channelID = channelID
        self.topic = topic
        self.length = length
        self.views = views
        self.img = img
        self.description = description
        self.upvotes = upvotes

    def __repr__(self):
        return '<id %r>' % self.id
