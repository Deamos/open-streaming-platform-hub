from .shared import db

class channels(db.Model):
    __tablename__ = "channels"
    id = db.Column(db.Integer, primary_key=True)
    hubID = db.Column(db.String(255), unique=True)
    serverID = db.Column(db.Integer, db.ForeignKey('servers.id'))
    name = db.Column(db.String(255))
    location = db.Column(db.String(255))
    streamer = db.Column(db.String(1024), db.ForeignKey('streamers.hubID'))
    topic = db.Column(db.String(1024), db.ForeignKey('topics.hubID'))
    protected = db.Column(db.Boolean)
    views = db.Column(db.Integer)
    currentViewers = db.Column(db.Integer)
    img = db.Column(db.String(255))
    description = db.Column(db.String(2056))

    videos = db.relationship('videos', backref='channel', lazy="joined")
    streams = db.relationship('streams', backref='channel', lazy="joined")

    def __init__(self, hubID, server, name, location, streamer, topic, protected, views, currentViewers, img, description):
        self.hubID = hubID
        self.serverID = server
        self.name = name
        self.location = location
        self.streamer = streamer
        self.topic = topic
        self.protected = protected
        self.views = views
        self.currentViewers = currentViewers
        self.img = img
        self.description = description

    def __repr__(self):
        return '<id %r>' % self.id
