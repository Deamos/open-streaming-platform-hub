from .shared import db

class streams(db.Model):
    __tablename__ = "streams"
    id = db.Column(db.Integer, primary_key=True)
    hubID = db.Column(db.String(255), unique=True)
    serverID = db.Column(db.Integer, db.ForeignKey('servers.id'))
    name = db.Column(db.String(255))
    streamer = db.Column(db.String(1024), db.ForeignKey('streamers.hubID'))
    channelID = db.Column(db.String(1024), db.ForeignKey('channels.hubID'))
    topic = db.Column(db.String(1024), db.ForeignKey('topics.hubID'))
    location = db.Column(db.String(255))
    views = db.Column(db.Integer)
    currentViewers = db.Column(db.Integer)
    img = db.Column(db.String(255))
    upvotes = db.Column(db.Integer)

    def __init__(self, hubID, server, name, location, streamer, topic, channelID, views, currentViewers, img, upvotes):
        self.hubID = hubID
        self.serverID = server
        self.name = name
        self.location = location
        self.streamer = streamer
        self.channelID = channelID
        self.topic = topic
        self.views = views
        self.currentViewers = currentViewers
        self.img = img
        self.upvotes = upvotes

    def __repr__(self):
        return '<id %r>' % self.id
