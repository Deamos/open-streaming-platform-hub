# Open Streaming Platform Hub

OSP Hub is current in development and is non-functional.

For more information on Open Streaming Platform visit (https://gitlab.com/Deamos/flask-nginx-rtmp-manager)

Thanks
----
Special thanks to the folks of the [OSP Discord channel](https://discord.gg/Jp5rzbD) and all contributors for their code, testing, and suggestions!

License
----

MIT License