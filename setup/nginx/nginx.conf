user  www-data;
worker_processes  4;

pid        /run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;

    keepalive_timeout  65;
    server {
        listen       80;

        # set client body size to 16M #
        client_max_body_size 16M;

        location / {
            proxy_pass http://127.0.0.1:5000;
            proxy_redirect     off;

            proxy_set_header   Host                 $host:$server_port;
            proxy_set_header   X-Real-IP            $remote_addr;
            proxy_set_header   X-Forwarded-For      $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Proto    $scheme;
        }

        # redirect server error pages to the static page /50x.html
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}
